@echo off
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
set "fullstamp=%YYYY%_%MM%_%DD%_%HH%%Min%%Sec%"

npx codeceptjs run --steps --reporter mochawesome --reporter-options reportPageTitle=Earn_Companion_E2E_Test_Report,reportTitle=Earn_Companion_E2E_Test_Report,reportDir=mochawesome-report/%fullstamp%