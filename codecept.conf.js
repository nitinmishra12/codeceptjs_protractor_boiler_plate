exports.config = {
  tests: "tests/*.test.js",
  output: './output',
  mocha: {
    reporterOptions: {
      reportDir: "output"
    }
  },
  helpers: {
    AssertWrapper: {
      require: "codeceptjs-assert"
    },
    Mochawesome: {
      uniqueScreenshotNames: "true"
    },
    REST: {
      endpoint: 'http://localhost:3000',
      onRequest: (request) => {
        //request.headers.auth = '123';
      }
    },
    GraphQL: {
      endpoint: "http://localhost:3000",
      defaultHeaders: {
        //'Auth': '11111',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    },
    Protractor: {
      url: 'https://google.com.au',
      driver: 'hosted',
      browser: 'chrome',
      angular: false,
      waitForTimeout: 10000,
      desiredCapabilities: {
        ieOptions: {
          'ie.browserCommandLineSwitches': '-private',
          'ie.usePerProcessProxy': true,
          'ie.ensureCleanSession': true,
          'ie.ignoreProtectedModeSettings': true,
          'ie.ignoreZoomSetting': true,
          'ie.nativeEvents': false,
          'ie.acceptSslCerts': true
        },
        chromeOptions: {
          args: ['--no-sandbox', '--window-size=320,568']
        }
      }
    },
    // Mochawesome: {
    //   uniqueScreenshotNames: true,
    //   stdout: "./output/console.log",
    //   options: {
    //     reportDir: "./output",
    //     reportFilename: "report"
    //   },
    // }
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  include: {
    I: "./steps_file.js"
  },
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
      './step_definitions/search_page.steps.js',
      './step_definitions/home_page.steps.js'
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    wdio: {
      enabled: true,
      services: ['selenium-standalone']
    }
  },
  multiple: {
    basic: {
      browsers: [
        {
          browser: "firefox",
          windowSize: "maximize",
          angular: false,
          desiredCapabilities: {
            acceptSslCerts: true,
            acceptInsecureCerts: true,
            ignoreHTTPSErrors: true,
            rejectUnauthorized: false
          }
        },
        {
          browser: "chrome",
          windowSize: "maximize",
          angular: false,
          desiredCapabilities: {
            acceptSslCerts: true
          }
        },
        {
          browser: "internet explorer",
          angular: false,
          desiredCapabilities: {
            ieOptions: {
              'ie.browserCommandLineSwitches': '-private',
              'ie.usePerProcessProxy': true,
              'ie.ensureCleanSession': true,
              'ie.ignoreProtectedModeSettings': true,
              'ie.ignoreZoomSetting': true,
              'ie.nativeEvents': false,
              'ie.acceptSslCerts': true
            }
          },
        },
      ]
    },
    basic_mobile: {
      browsers: [
        {
          browser: "chrome",
          chromeOptions: {
            args: ['show-fps-counter=true'],
            mobileEmulation : {
              deviceName: 'Apple iPhone 5'
            }
          },
          windowSize: "568x320",
          angular: false,
          defaultViewport: {
            "width": 568,
            "height": 320,
            "deviceScaleFactor": 1,
            "isMobile": true,
            "hasTouch": true,
            "isLandscape": false
          },
          desiredCapabilities: {
            acceptSslCerts: true
          }
        },
      ]
    },
  },
  name: 'Workspace_codeceptJS_Protractor',
  onPrepare: () => {
    return browser.getProcessedConfig().then(function (config) {
      if (config.specs.length > 0) {
        const spec = path.basename(config.specs[0]);

        process.env.MOCHAWESOME_REPORTTITLE = spec;
        process.env.MOCHAWESOME_REPORTFILENAME = spec;
      }
    });
  }
}