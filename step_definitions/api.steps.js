const expect = require('chai').expect;
const {I} = inject();
let res;

Given('Get is requested', async () => {
    res = await I.sendGetRequest('/todos');
});

When('API is loaded', async () => {
    expect(res.status).to.eql(200);
});

Then('First ID is 1', async () => {
    expect(res.data[0].id).to.eql(1);
    console.log('ID is  ==> '+res.data[0].id);
});
