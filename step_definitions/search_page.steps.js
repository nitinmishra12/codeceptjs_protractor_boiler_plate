const { I } = inject();

Then('the user is landed on search page', () => {
    I.see('SafeSearch on')
});

Then('the user is shown with search result for postcode', () => {
    I.see('2000 - Wikipedia')
});

Then('the user is shown with search result for state', () => {
    I.see('NSW Government')
});