const { I } = inject();
let card_details = require('../data/card_details')
global.card_type = card_details.amex_ultimate
let search = require('../data/search_data')
global.postcode = search.search.postcode
global.state = search.search.state

let home_pg = require('../page_objects/home_page')
let search_box = home_pg.home_page.search_box;
let search_button = home_pg.home_page.search_button;

Given("user opens google.com", () => {
    I.amOnPage('/');
});

When("user enters the post code", () => {
    I.fillField(search_box, postcode);
});

When("user enters the state", () => {
    I.fillField(search_box, state);
});

When("user clicks on search button", () => {
    I.click(search_button);
});