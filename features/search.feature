@regression
Feature: Search on Google
  test google search

  Scenario: search for postcode
    Given user opens google.com
    When user enters the post code
    When user clicks on search button
    Then the user is landed on search page
    Then the user is shown with search result for postcode

  Scenario: search for state
    Given user opens google.com
    When user enters the state
    When user clicks on search button
    Then the user is landed on search page
    Then the user is shown with search result for state