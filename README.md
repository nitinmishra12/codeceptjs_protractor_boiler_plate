## Description
     This test framework consist of codecept JS

## Tech Stack
NodeJS	--> https://nodejs.org/en/download/
CodeceptJS	--> https://codecept.io/ 
Protractor	--> https://codecept.io/helpers/Puppeteer/#configuration
mocahawesome reporter	--> https://www.npmjs.com/package/mochawesome-report-generator

## Framework structure
data	--> test data is saved in this folder
features	--> feature files are saved here for BDD
mocahwesome-report	--> reports are stored inside this folder within another folder named with the current timestamp
node_modules	--> node modules are saved here by default
page_objects	--> page objects/ locators are kept under this folder
step_definitions	--> step definition files re kept under this folder
.gitnore	--> ignores the files/ folders which are not supposed to be checked in
codecept.conf.js	--> this is a config file for codeceptJS to set the entry point, multi-browser, etc
package-lock.json	--> default file
package.json	--> the file used to set the config for techstack installed and run commands
steps_file.ts	--> custom steps file


## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## Now, open cmd and got to the project folder and then install the below items

# NodeJS
- Download Node JS and install --> `https://nodejs.org/en/download/`

# NPM
- Install command --> `npm install`

# CodeceptJS and Protractor
- Install codeceptJS and Protractor with qucikstart steps --> `https://codecept.io/quickstart/`

# Mochawesome Report Generator
- Install command --> `npm i mochawesome-report-generator`

# JSON server
- Install command --> `npm install -g json-server`
- Run command --> `json-server --watch db.json`